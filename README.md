# iCal Tool

#### 符合RFC-5545標準之iCalendar元件

## Dependencies
- .NETStandard 2.0
- Ical.Net (>= 4.1.9)

## Method

#### 以下Create、Update、Stop、GetScheduleStatus、GetNextExecutionTimeSpan方法皆可用iCalTool靜態方法(帶入identity)或是iCalTool實體物件(不需使用identity)的方式來存取。

#### **設定ics檔案的資料夾相對路徑(預設為執行目錄下之ics資料夾)**
> iCalTool.ICSRelativeFolderPath

#### **新增iCal元件並執行**
> iCalTool.Create(identity).AddEventTrigger(In, Out).Start();
> - identity: iCal唯一識別碼
> - In: 進入執行區間時觸發的function
> - Out: 離開執行區間時觸發的function

#### **取得iCal元件目前狀態**
> iCalTool.GetScheduleStatus(identity);
> - identity: iCal唯一識別碼
> - 回傳值: Status.Unknown(未知)、Status.In(在執行區間之內)、Status.Out(在執行區間之外)

#### **取得iCal元件距離下一個執行區間還有多久的時間間距**
> iCalTool.GetNextExecutionTimeSpan(identity);
> - identity: iCal唯一識別碼
> - 回傳值: TimeSpan

#### **更新iCal所運行的ics內容**
> iCalTool.Update(identity, ics).Start();
> - identity: iCal唯一識別碼
> - ics: ics內容

#### **停止並移除iCal元件**
> iCalTool.Stop(identity);
> - identity: iCal唯一識別碼