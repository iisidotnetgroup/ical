﻿using iCal.Tool;
using System;

namespace iCal.NF.TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            iCalTool.Create("test01").AddEventTrigger(In, Out).Start();
            //iCalTool.Update("test01", "ical string").Start();
            //iCalTool.Stop("test01");

            Console.ReadKey();
        }

        static void In(iCalTool ical)
        {
            Console.WriteLine($"{DateTime.Now} {ical.Identity} In");
        }

        static void Out(iCalTool ical)
        {
            Console.WriteLine($"{DateTime.Now} {ical.Identity} Out");
        }
    }
}
