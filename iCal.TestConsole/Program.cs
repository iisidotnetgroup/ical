﻿using iCal.Tool;
using System;
using System.IO;

namespace iCal.TestConsole
{
    class Program
    {
        static string ics = @"";
        static string identity = "test01";

        static void Main(string[] args)
        {
            //iCalTool.Create(identity).AddEventTrigger(In, Out).Start();
            iCalTool.DefaultICSIsActivated = true;

            while (true)
            {
                var keyInfo = Console.ReadKey();

                if (keyInfo.Key == ConsoleKey.A)
                {
                    iCalTool.Create(identity).AddEventTrigger(In, Out).Start();
                }
                else if (keyInfo.Key == ConsoleKey.P)
                {
                    Console.WriteLine(iCalTool.GetNextExecutionTimeSpan(identity));
                }
                else if (keyInfo.Key == ConsoleKey.U)
                {
                    using (TextReader reader = File.OpenText("ics/test01_update.ics"))
                    {
                        ics = reader.ReadToEnd();
                    }

                    iCalTool.Update(identity, ics, true);
                }
                else if (keyInfo.Key == ConsoleKey.S)
                {
                    iCalTool.Stop(identity);
                }
            }

            iCalTool.Stop("test01");
        }

        static void In(iCalTool ical)
        {
            Console.WriteLine($"{DateTime.Now} {ical.Identity} In");
        }

        static void Out(iCalTool ical)
        {
           Console.WriteLine($"{DateTime.Now} {ical.Identity} Out");
        }
    }
}
