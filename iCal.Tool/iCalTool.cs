﻿using Ical.Net;
using Ical.Net.DataTypes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace iCal.Tool
{
    public delegate void TriggerFunction(iCalTool ical);

    public enum Status { Unknown, In, Out }

    public class iCalTool
    {
        #region public member

        /// <summary>
        /// ICS檔案的資料夾路徑。
        /// </summary>
        public static string ICSRelativeFolderPath
        {
            get
            {
                return icsRelativeFolderPath;
            }
            set
            {
                icsRelativeFolderPath = value;
                icsAbsoluteFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, icsRelativeFolderPath);
            }
        }

        /// <summary>
        /// 預設的ICS檔案是否為可執行狀態。預設為false。
        /// </summary>
        public static bool DefaultICSIsActivated
        {
            get
            {
                return defaultICSIsActivated;
            }
            set
            {
                defaultICSIsActivated = value;

                if(defaultICSIsActivated)
                    Console.WriteLine($"iCalTool: AutoCreate Mode");
                else
                    Console.WriteLine($"iCalTool: ManualCreate Mode");
            }
        }

        public static bool ShowDebug { get; set; } = false;

        public string Identity { get; private set; }
        public Status Status { get; private set; }

        #endregion

        #region private member

        private static readonly string autoCreateModeICS;
        private static readonly string manualCreateModeICS;
        private static string icsRelativeFolderPath = "ics";
        private static string icsAbsoluteFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, icsRelativeFolderPath);
        private static Dictionary<string, iCalTool> currentTools = new Dictionary<string, iCalTool>();
        private static object globalLocker = new object();
        private static Timer createCalendarTimer;
        private static int createCalendarTimerMS = (int)TimeSpan.FromMinutes(2).TotalMilliseconds;
        private static int checkCalendarTimerMS = (int)TimeSpan.FromSeconds(3).TotalMilliseconds;
        private static bool defaultICSIsActivated = false;
        private static Random random;

        private Timer checkCalendarTimer;
        private TriggerFunction inExecutionInterval;
        private TriggerFunction outofExecutionInterval;
        private Calendar currentCalendar;
        private Occurrence currentOccurrence;
        private HashSet<Occurrence> currentOccurrences;
        private object privateLocker = new object();

        #endregion

        #region ctor

        iCalTool(string identity)
        {
            Identity = identity;
        }

        static iCalTool()
        {
            random = new Random();

            //產生預設就會執行的ics檔案
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("iCal.Tool.auto.ics"))
            {
                using (var reader = new StreamReader(stream))
                {
                    autoCreateModeICS = reader.ReadToEnd();
                }
            }

            //產生預設永遠不會執行的ics檔案
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("iCal.Tool.manual.ics"))
            {
                using (var reader = new StreamReader(stream))
                {
                    manualCreateModeICS = reader.ReadToEnd();
                }
            }

            createCalendarTimer = new Timer(CreateCalenderTimerCallback);
            createCalendarTimer.Change(createCalendarTimerMS, createCalendarTimerMS);
        }

        #endregion

        /// <summary>
        /// Create a new iCalTool with identity.
        /// </summary>
        /// <param name="identity">ID of iCalTool.</param>
        /// <returns></returns>
        public static iCalTool Create(string identity)
        {
            if (string.IsNullOrWhiteSpace(identity))
                throw new ArgumentNullException("identity");

            lock (globalLocker)
            {
                if (currentTools.ContainsKey(identity))
                    throw new Exception($"Tool with identity '{identity}' exists.");

                iCalTool tool = new iCalTool(identity);
                currentTools.Add(identity, tool);

                return tool;
            }
        }

        /// <summary>
        /// Update scheduler.
        /// </summary>
        /// <param name="icsContent">ICS content.</param>
        /// <param name="runSchedule">Start schedule immediately.</param>
        public void Update(string icsContent, bool runSchedule = false)
        {
            Update(Identity, icsContent);
        }

        /// <summary>
        /// Update scheduler with identity.
        /// </summary>
        /// <param name="identity">ID of iCalTool.</param>
        /// <param name="icsContent">ICS content.</param>
        /// <param name="runSchedule">Start schedule immediately.</param>
        /// <returns></returns>
        public static void Update(string identity, string icsContent, bool runSchedule = false)
        {
            if (string.IsNullOrWhiteSpace(identity))
                throw new ArgumentNullException("identity");

            if (string.IsNullOrWhiteSpace(icsContent))
                throw new ArgumentNullException("icsContent");

            string icsFilePath = GetICSFilePath(identity);
            File.WriteAllText(icsFilePath, icsContent);

            if (runSchedule)
            {
                iCalTool tool = GetTool(identity);

                if (tool == null)
                    tool = Create(identity);

                tool.Start();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inExecutionInterval"></param>
        /// <param name="outofExecutionInterval"></param>
        /// <returns></returns>
        public iCalTool AddEventTrigger(TriggerFunction inExecutionInterval, TriggerFunction outofExecutionInterval)
        {
            this.inExecutionInterval = inExecutionInterval ?? throw new ArgumentNullException("inExecutionInterval");
            this.outofExecutionInterval = outofExecutionInterval ?? throw new ArgumentNullException("outofExecutionInterval");

            return this;
        }

        /// <summary>
        /// Start scheduler.
        /// </summary>
        /// <returns></returns>
        public iCalTool Start()
        {
            string icsFilePath = GetICSFilePath(Identity);
            Calendar ical;

            try
            {
                using (TextReader reader = File.OpenText(icsFilePath))
                {
                    ical = Calendar.Load(reader);
                    ical.RecurrenceRestriction = RecurrenceRestrictionType.NoRestriction;
                }
            }
            catch (Exception error)
            {
                throw error;
            }

            lock (privateLocker)
            {
                Status = Status.Unknown;
                checkCalendarTimer?.Dispose();
                currentCalendar = null;
                currentOccurrence = null;
            }

            if (ical != null)
            {
                currentCalendar = ical;
                CreateCalender();

                checkCalendarTimer = new Timer(CheckCalenderTimerCallback);
                checkCalendarTimer.Change(0, checkCalendarTimerMS);
            }

            return this;
        }

        /// <summary>
        /// Start scheduler with identity.
        /// </summary>
        /// <param name="identity">ID of iCalTool.</param>
        /// <returns></returns>
        public static void Start(string identity)
        {
            iCalTool tool = GetTool(identity);
            tool?.Start();
        }

        /// <summary>
        /// Get the timespan from now to next job execution.
        /// </summary>
        /// <param name="identity">ID of iCalTool.</param>
        /// <returns></returns>
        public static TimeSpan GetNextExecutionTimeSpan(string identity)
        {
            iCalTool tool = GetTool(identity);

            if (tool == null)
                return TimeSpan.Zero;

            return tool.GetNextExecutionTimeSpan();
        }

        /// <summary>
        /// Get the timespan from now to next job execution.
        /// </summary>
        /// <returns></returns>
        public TimeSpan GetNextExecutionTimeSpan()
        {
            DateTime now = GetNowDatetime();

            lock (privateLocker)
            {
                var nextOccurrence = currentOccurrences?.FirstOrDefault(item => now <= item.Period.StartTime.AsUtc && now <= item.Period.EndTime.AsUtc);

                if(nextOccurrence == null)
                    nextOccurrence = currentCalendar?.GetOccurrences(now, now.AddDays(1)).FirstOrDefault(item => now <= item.Period.StartTime.AsUtc && now <= item.Period.EndTime.AsUtc);

                if (nextOccurrence != null)
                {
                    return nextOccurrence.Period.StartTime.AsUtc - now;
                }
                else
                {
                    //Next occurrence is longer than one day
                    return TimeSpan.FromDays(1);
                }
            }
        }

        /// <summary>
        /// Get the job schedule status with identity.
        /// </summary>
        /// <param name="identity">ID of iCalTool.</param>
        /// <returns></returns>
        public static Status GetScheduleStatus(string identity)
        {
            iCalTool tool = GetTool(identity);

            if (tool == null)
                return Status.Unknown;

            return tool.Status;
        }

        /// <summary>
        /// Get the job schedule status.
        /// </summary>
        /// <returns></returns>
        public Status GetScheduleStatus()
        {
            return Status;
        }

        /// <summary>
        /// Stop and remove scheduler.
        /// </summary>
        public void Stop()
        {
            lock (globalLocker)
            {
                currentTools.Remove(Identity);
            }

            lock (privateLocker)
            {
                Status = Status.Unknown;
                checkCalendarTimer?.Dispose();
                checkCalendarTimer = null;
                currentCalendar = null;
                currentOccurrence = null;
                currentOccurrences?.Clear();
                currentOccurrences = null;
            }
        }

        /// <summary>
        /// Stop and remove scheduler with identity.
        /// </summary>
        public static void Stop(string identity)
        {
            iCalTool tool = GetTool(identity);
            tool?.Stop();
        }

        private static string GetICSFilePath(string identity)
        {
            if (!Directory.Exists(icsAbsoluteFolderPath))
            {
                //throw new DirectoryNotFoundException($"ICS file folder {icsAbsoluteFolderPath} doesn't exist.");

                Console.WriteLine($"ICS file folder {icsAbsoluteFolderPath} doesn't exist. Try creating a ics file folder...");
                Directory.CreateDirectory(icsAbsoluteFolderPath);
            }

            string icsFilePath = Path.Combine(icsAbsoluteFolderPath, $"{identity}.ics");

            if (!File.Exists(icsFilePath))
            {
                //throw new FileNotFoundException($"ICS file {icsFilePath} doesn't exist.");

                string mode = DefaultICSIsActivated ? "auto_create_mode" : "manual_create_mode";

                Console.WriteLine($"ICS file {icsFilePath} doesn't exist. Try creating a {mode} ics content to {icsFilePath}...");

                string icsContent = string.Empty;

                if (DefaultICSIsActivated)
                    icsContent = autoCreateModeICS; //產生預設就會執行的ics檔案
                else
                    icsContent = manualCreateModeICS; //產生預設永遠不會執行的ics檔案

                //亂數產生開始執行時間
                int randomTime = random.Next(10, 59);
                icsContent = icsContent.Replace("0101T000000", $"0101T0000{randomTime}");

                File.WriteAllText(icsFilePath, icsContent);
            }

            return icsFilePath;
        }

        private static void CreateCalenderTimerCallback(object parameter)
        {
            lock (globalLocker)
            {
                if (currentTools.Count == 0)
                    return;

                foreach (iCalTool tool in currentTools.Values)
                {
                    tool.CreateCalender();
                }
            }
        }

        private void CreateCalender()
        {
            DateTime now = GetNowDatetime();

            lock (privateLocker)
            {
                currentOccurrences = currentCalendar?.GetOccurrences(now, now.AddMinutes(4));
            }
        }

        private void CheckCalenderTimerCallback(object parameter)
        {
            DateTime now = GetNowDatetime();

            lock (privateLocker)
            {
                var firstOcc = currentOccurrences.FirstOrDefault();
                var lastOcc = currentOccurrences.LastOrDefault();

                if (ShowDebug)
                {
                    Console.WriteLine($"Check  {Identity} (occ count: {currentOccurrences.Count})...");
                    Console.WriteLine($"{Identity} T now: {now.ToString("yyyy-MM-ddTHH:mm:ss.fff")}");
                    Console.WriteLine($"{Identity} F occ: {GetString(firstOcc?.Period.StartTime)} ~ {GetString(firstOcc?.Period.EndTime)}");
                    Console.WriteLine($"{Identity} L occ: {GetString(lastOcc?.Period.StartTime)} ~ {GetString(lastOcc?.Period.EndTime)}");
                    Console.WriteLine($"{Identity} C occ: {GetString(currentOccurrence?.Period.StartTime)} ~ {GetString(currentOccurrence?.Period.EndTime)}");
                }

                var occurrence = currentOccurrences?.FirstOrDefault(item => now >= item.Period.StartTime.AsUtc && now <= item.Period.EndTime.AsUtc);

                if (occurrence != null)
                {
                    if (ShowDebug)
                        Console.WriteLine($"{Identity} N occ: {GetString(occurrence?.Period.StartTime)} ~ {GetString(occurrence?.Period.EndTime)}");

                    if (currentOccurrence != occurrence)
                    {
                        Status = Status.In;
                        currentOccurrence = occurrence;

                        AddToThreadPool(inExecutionInterval);

                        double dueInterval_ms = (currentOccurrence.Period.EndTime.AsUtc - now).TotalMilliseconds;
                        int interval = Convert.ToInt32(dueInterval_ms) + 15;
                        checkCalendarTimer?.Change(interval, interval);
                    }
                    else
                    {
                        checkCalendarTimer?.Change(1, 1);

                        if (ShowDebug)
                            Console.WriteLine("{Identity} In same occ");
                    }
                }
                else
                {
                    if (Status != Status.Out)
                        AddToThreadPool(outofExecutionInterval);

                    Status = Status.Out;
                    currentOccurrence = null;

                    //get next occurrence
                    var nextOccurrence = currentOccurrences?.FirstOrDefault(item => now <= item.Period.StartTime.AsUtc && now <= item.Period.EndTime.AsUtc);

                    if (nextOccurrence != null)
                    {
                        double nextOccurrenceInterval_ms = (nextOccurrence.Period.StartTime.AsUtc - now).TotalMilliseconds;
                        double nextOccurrenceInterval_s = TimeSpan.FromMilliseconds(nextOccurrenceInterval_ms).TotalSeconds;
                        double nextOccurrenceInterval_m = TimeSpan.FromMilliseconds(nextOccurrenceInterval_ms).TotalMinutes;

                        int interval = Convert.ToInt32(nextOccurrenceInterval_ms) + 15;
                        checkCalendarTimer?.Change(interval, interval);

                        if (ShowDebug)
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine($"Calendar '{Identity}' remains {Math.Round(nextOccurrenceInterval_ms, 2)}ms ({Math.Round(nextOccurrenceInterval_s, 2)}s) ({Math.Round(nextOccurrenceInterval_m, 2)}m) to trigger next occurrence.");
                            Console.ResetColor();
                        }
                    }
                    else
                    {
                        int interval = Convert.ToInt32(TimeSpan.FromMinutes(3).TotalMilliseconds);
                        checkCalendarTimer?.Change(interval, interval);

                        if (ShowDebug)
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine($"Calendar '{Identity}' wait for 3 minutes to check next occurrence again.");
                            Console.ResetColor();
                        }
                    }
                }
            }
        }

        private static iCalTool GetTool(string identity)
        {
            if (string.IsNullOrWhiteSpace(identity))
                throw new ArgumentNullException("identity");

            lock (globalLocker)
            {
                currentTools.TryGetValue(identity, out iCalTool tool);

                //if (!currentTools.TryGetValue(identity, out tool))
                //    throw new Exception($"Tool with identity '{identity}' doesn't exist.");

                return tool;
            }
        }

        private static DateTime GetNowDatetime()
        {
            return DateTime.UtcNow;
        }

        private void AddToThreadPool(TriggerFunction func)
        {
            if (func != null)
            {
                ThreadPool.QueueUserWorkItem(item =>
                {
                    func(this);
                });


                //ThreadPool.GetAvailableThreads(out int at, out int iot);

                //Console.ForegroundColor = ConsoleColor.Red;
                //Console.WriteLine($"ThreadPool avaliable thread:{at}");
                //Console.ResetColor();
            }
        }

        private static string GetString(IDateTime dt)
        {
            return dt?.Value.ToString("yyyy-MM-ddTHH:mm:ss.fff");
        }
    }
}